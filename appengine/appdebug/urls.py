from django.urls import path

from . import views

app_name = 'appdebug'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('dump/', views.DumpHeadersView.as_view(), name='dump'),
]
