from django.apps import AppConfig


class AppDebugConfig(AppConfig):
    name = 'appdebug'
