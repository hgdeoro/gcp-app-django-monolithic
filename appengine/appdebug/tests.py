from django.test import TestCase  # noqa: 401
from django.test import Client


class SimpleTest(TestCase):
    def test_default_view(self):
        client = Client()
        response = client.get('/')
        self.assertTrue(len(response.content) > 0)
