import os
import random

from django.core.serializers.json import DjangoJSONEncoder
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpRequest  # noqa: 401
from django.views import generic


class IndexView(generic.View):
    def get(self, request, *args, **kwargs):
        response = JsonResponse({'value': random.randint(0, 1_000_000)})
        return response


class CustomEncoder(DjangoJSONEncoder):
    def __init__(self, *args, **kwargs):
        kwargs.pop('default', None)
        super().__init__(*args, default=str, **kwargs)


class DumpHeadersView(generic.View):
    def get(self, request: HttpRequest, *args, **kwargs):
        data = {
            'request.COOKIES': request.COOKIES,
            'request.headers': dict(request.headers),
            'request.META': request.META,
            'os.environ': dict(os.environ),
        }
        response = JsonResponse(data, encoder=CustomEncoder)
        return response

    def post(self, request: HttpRequest, *args, **kwargs):
        data = {
            'request.COOKIES': request.COOKIES,
            'request.headers': dict(request.headers),
            'request.META': request.META,
            'os.environ': dict(os.environ),
        }
        response = JsonResponse(data, encoder=CustomEncoder)
        return response
