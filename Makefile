.PHONY: help
.ONESHELL:

# For use of bash, to be able to "source" virtualenv's "activate" script
SHELL = /bin/bash

VIRTUALENV_DIR := .virtualenv
ROOT_CLOUDSDK_ACTIVE_CONFIG_NAME ?= 'default'

help:
	@echo "See the sources"

virtualenv-update:
	set -e
	test -d $(VIRTUALENV_DIR) || virtualenv -p python3.7 $(VIRTUALENV_DIR)
	source $(VIRTUALENV_DIR)/bin/activate
	pip install pip-tools

# appengine/requirements.txt requirements-dev.txt: requirements.in requirements-dev.in
pip-compile-sync:
	set -e
	source $(VIRTUALENV_DIR)/bin/activate
	pip-compile --output-file appengine/requirements.txt  	requirements.in
	pip-compile --output-file requirements-dev.txt 			requirements.in requirements-dev.in
	pip-sync requirements-dev.txt

uwsgi:
	set -e
	source $(VIRTUALENV_DIR)/bin/activate
	uwsgi --ini uwsgi.ini

docker-compose-up:
	set -e
	source $(VIRTUALENV_DIR)/bin/activate
	docker-compose --env-file /dev/null -f stack.yaml up

#appengine-deploy:
#	set -e
#	PROJECT=$$(jq -r .projectId < .config/gcp-project.json)
#	gcloud \
#		--project="$${PROJECT}" \
#		app deploy appengine/
#
#appengine-logs-tail:
#	set -e
#	PROJECT=$$(jq -r .projectId < .config/gcp-project.json)
#	gcloud \
#		--project="$${PROJECT}" \
#		app logs tail -s default
#
#appengine-app-browse:
#	set -e
#	PROJECT=$$(jq -r .projectId < .config/gcp-project.json)
#	gcloud \
#		--project="$${PROJECT}" \
#		app browse --project="$${PROJECT}"
