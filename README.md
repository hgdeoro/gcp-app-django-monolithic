# Backend App on GCP

This is part of a multi-project idea:

* [gcp-devops](https://gitlab.com/hgdeoro/gcp-devops).
* [gcp-app-django-monolithic](https://gitlab.com/hgdeoro/gcp-app-django-monolithic).

## What's here

* very simplistic REST API backend

### Work in progress

* migrate App to Django + DRF
* use [cookiecutter-django](https://github.com/pydanny/cookiecutter-django)

# Docs and References

* GCP
   * [AppEngine + Python3 + Django](https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard_python37/django)
